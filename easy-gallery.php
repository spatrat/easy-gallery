<?php
/*
Plugin Name: Easy Gallery
Description: Create galleries for your wordpress site easyly
Version: 1
Author: Simon Patrat
Author URI: simonpatrat.me
License: GPL
Copyright: Simon Patrat
*/

function SP_easygallery_setup_post_type() {

    // Define args
    $labels = array(
		'name'                  => _x( 'Gallery', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Gallery Item', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Gallery', 'text_domain' ),
		'name_admin_bar'        => __( 'Gallery', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Attributs de l\'item', 'text_domain' ),
		'all_items'             => __( 'Tous les items', 'text_domain' ),
		'add_new_item'          => __( 'Ajouter un nouvel item', 'text_domain' ),
		'add_new'               => __( 'Ajouter un nouvel item', 'text_domain' ),
		'new_item'              => __( 'Nouvel item', 'text_domain' ),
		'edit_item'             => __( 'Éditer l\'item', 'text_domain' ),
		'update_item'           => __( 'Mettre à jour l\'item', 'text_domain' ),
		'view_item'             => __( 'Voir l\'item', 'text_domain' ),
		'view_items'            => __( 'Voir les items', 'text_domain' ),
		'search_items'          => __( 'Chercher l\'item', 'text_domain' ),
		'not_found'             => __( 'Pas trouvé', 'text_domain' ),
		'not_found_in_trash'    => __( 'Pas trouvé dans la corbeille', 'text_domain' ),
		'featured_image'        => __( 'Image de l\'élément', 'text_domain' ),
		'set_featured_image'    => __( 'Ajouter l\'image principale', 'text_domain' ),
		'remove_featured_image' => __( 'Enlever l\'image principale', 'text_domain' ),
		'use_featured_image'    => __( 'Utiliser comme image principale de l\'item', 'text_domain' ),
		'insert_into_item'      => __( 'Insérer dans l\'item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Liste d\'items', 'text_domain' ),
		'items_list_navigation' => __( 'Navigation de la liste des items', 'text_domain' ),
		'filter_items_list'     => __( 'Filtre de la liste des items', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Item de la galerie', 'text_domain' ),
		'description'           => __( 'Éléments de la galerie', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'revisions', 'custom-fields' ),
		'taxonomies'            => array( 'gallery' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true, // TODO set to true and then add the capability to chosse post type and instanciate galleries
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-images-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);

    // register the "eg-item" custom post type
    register_post_type( 'eg-item', $args);
}

function SP_easygallery_assets() {

    function SP_easygallery_register_and_enqueue_assets() {
        wp_enqueue_style( 'easy-gallery-css', plugins_url( '/css/easy-gallery.css', __FILE__ ) );
        wp_register_script('masonry', '//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', null, true);
        wp_enqueue_script('masonry');
        wp_enqueue_script( 'easy-gallery-js', plugins_url( '/js/easy-gallery.js', __FILE__ ));
    }

    add_action('wp_enqueue_scripts','SP_easygallery_register_and_enqueue_assets');
}

function SP_easygallery_css() {

}

function SP_easygallery_shortcode($atts) {
        global $wp_query,
        $post;

    $atts = shortcode_atts( array(
        'nb_items' => 40,
        'post_type' => 'post',
    ), $atts );

    $loop = new WP_Query( array(
        'posts_per_page'    => $atts['nb_items'],
        // TODO eg-item post type and add the possibility to select the post type (then pass it to shortcode)
        'post_type'         => $atts['post_type'],
/*         'orderby'           => 'menu_order',
        'order'             => 'ASC', */
    ) );

    if( ! $loop->have_posts() ) {
        return false;
    }

    function SP_easygallery_get_gallery_content ($loop) {

        $loop_content = '';

        while( $loop->have_posts() ) {
            $loop->the_post();
            $post_id = get_the_ID();
            $large_image_url = get_the_post_thumbnail_url($post_id, 'large');
            $medium_image_url = get_the_post_thumbnail_url($post_id, 'SP_easygallery_medium');
            $post_title = get_the_title($post_id);
            $post_link = get_the_permalink($post_id);
            $post_index = $loop -> current_post;
            // TODO: Add dynamic readmore link
            $content = '<div
                            class="sp-easygallery__item col col-sm-6 col-md-4"
                            data-large-image-url="'. $large_image_url .'"
                            data-title="'. $post_title .'"
                            data-index="' . $post_index . '"
                            data-postlink="' . $post_link . '"
                            dat-postlinktext="En savoir plus"
                            tabindex="0"
                        >
                            <div class="sp-easygallery__image">
                                <img src="'. $medium_image_url .'" alt="'. $post_title .'">
                            </div>
                            <div class="sp-easygallery__information">
                                <div class="sp-easygallery__title">
                                    '. $post_title .'
                                </div>
                            </div>
                        </div>';
            $loop_content .= $content;
        }

        return $loop_content;
    }

    $gallery = '
        <div class="sp-easygallery js-sp-easy-gallery row">
            ' . SP_easygallery_get_gallery_content($loop) . '
        </div>
    ';

    return $gallery;

    wp_reset_postdata();
}

function SP_easygallery_add_image_size() {
    add_image_size( 'SP_easygallery_medium', 480 );
}

function SP_easygallery_add_shortcode() {
    $tag = 'Gallery';
    $func = 'SP_easygallery_shortcode';
    add_shortcode($tag, $func);
}

function SP_easygallery_init() {
    SP_easygallery_add_image_size();
    SP_easygallery_setup_post_type();
    SP_easygallery_add_shortcode();
    SP_easygallery_assets();
}

add_action( 'init', 'SP_easygallery_init');

function SP_easygallery_install() {
    // trigger our function that registers the custom post type
    SP_easygallery_setup_post_type();

    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'SP_easygallery_install' );

function SP_easygallery_deactivation() {
    // unregister the post type, so the rules are no longer in memory
    unregister_post_type( 'eg-item' );
    // clear the permalinks to remove our post type's rules from the database
    flush_rewrite_rules();
}
(function ($) {
    var gallerySelector = '.js-sp-easy-gallery';
    var galleryItemSelector = '.sp-easygallery__item';

    $.fn.loader = function (options) {
        var defaults = {
            text: 'Loading...',
            targetSelector: null,
            displayText: false,
            containerMinHeight: '300px',
        };
        var $element = $(this);
        var settings = $.extend({}, defaults, options);
        var $target = settings.targetSelector ? $(settings.targetSelector) : $element;
        var $loader = $('<div class="sp-loader" aria-label="' + settings.text + '"></div>');
        var $loaderContentWrapper = $('<div class="sp-loader__content-wrapper"></div>');
        var $loaderWithoutTextContent = $(
            '<div class="sp-loader__element"></div>' +
            '<div class="sp-loader__element"></div>' +
            '<div class="sp-loader__element"></div>'
        );
        $loaderWithTextContent = $('<div class="sp-loader-text-container"></div>');
        var init = function () {

            var text = settings.text;
            var displayText = settings.displayText;
            if (displayText) {
                $loaderWithTextContent.text(text);
                $loaderContentWrapper.append($loaderWithTextContent);
            } else {
                $loaderContentWrapper.append($loaderWithoutTextContent);
            };
            $loader.append($loaderContentWrapper);
            $loader.css('min-height', settings.containerMinHeight);
            $target.prepend($loader);
        }

        $.fn.loader.removeLoader = function () {
            if (!$loader) { return; }
            $loader.remove();
        }

        init();

        return this;
    }

    $.fn.easyGalleryViewBox = function (options) {
        var defaults = {
            isWithThumbnails: false,
            autoDisplayViewBoxWithURLChange: false,
        };
        var settings = $.extend({}, defaults, options);
        var galleryItems = this;
        var $viewBoxWrapper = $('<div class="sp-easy-gallery-viewbox" tabindex="-1"></div>');
        var $viewBoxCloseButton = $('<button type="button" class="sp-easy-gallery-viewbox__close" aria-label="Close this viewbox"></button>');
        var $viewBoxWrapperInner = $('<div class="sp-easy-gallery-viewbox__inner"></div>');
        var $viewBoxImageContainer = $('<div class="sp-easy-gallery-viewbox__image-container"></div>');
        var $img = $('<img src="" alt="" class="sp-easy-gallery-viewbox__image" />');
        var $title = $('<div class="sp-easy-gallery-viewbox__item__title"></div>');
        var $itemInformationsContainer = $('<div class="sp-easygallery__post-links-container"></div>');
        var $postLinkButton = $('<a href="" class="sp-easygallery__post-link--button">En savoir plus</a>');
        var $itemInformations = $itemInformationsContainer.append($postLinkButton);

        var getAlreadyDisplayedViewBox = function () {
            return $('.sp-easy-gallery-viewbox');
        };
        var gallery = {
            currentItem: null,
        };

        $(document).on('easyGalleryViewBox:changeState', function () {
            if (gallery.currentItem) {
                $('.sp-easygallery-viewbox__navigation__menu__item')
                    .removeClass('active');
                $('.sp-easygallery-viewbox__navigation__menu__item button')
                    .removeClass('active');
                var currentItemIndex = gallery.currentItem.data('index');
                var $activeButton = $('.sp-easygallery-viewbox__navigation__menu__item button[data-target="' + currentItemIndex + '"]');
                $activeButton.addClass('active');
                $activeButton
                    .closest('.sp-easygallery-viewbox__navigation__menu__item')
                    .addClass('active');
            }
        });

        function changeState(newState) {
            gallery = $.extend({}, gallery, newState);
            $(document).trigger('easyGalleryViewBox:changeState');
        }

        function mobileHooks() {
            var isMobile = $('#wpadminbar').hasClass('mobile');
            if (isMobile) {
                $('.sp-easy-gallery-viewbox').addClass('is-mobile');
            } else {
                $('.sp-easy-gallery-viewbox').removeClass('is-mobile');
            }
        }

        function closeViewBox() {
            var $viewBoxToClose = $('.sp-easy-gallery-viewbox');
            $viewBoxToClose.fadeOut(200, function () {
                $viewBoxWrapper.empty().detach();
                $viewBoxWrapperInner.empty().detach();
                $viewBoxImageContainer.empty().detach();
                $viewBoxCloseButton.detach();
                $img.detach();

                $('body')
                    .removeClass('overflow-hidden')
                    .css('overflow', '');
                $viewBoxCloseButton.off('click');
            });
        }

        function onClickCloseButton() {
            closeViewBox();
        }


        function changeImage(buttonData) {
            var $galleryItems = $(galleryItems);
            var currentItemIndex = $('.sp-easy-gallery-viewbox img.sp-easy-gallery-viewbox__image').data('index');
            switch (buttonData.type) {
                case 'next':
                    var nextIndex = currentItemIndex + 1 < $galleryItems.length
                        ? currentItemIndex + 1
                        : 0;
                    var nextItem = $galleryItems.filter(function () {
                        return $(this).data('index') === nextIndex
                    });

                    var imageUrl = nextItem.data('large-image-url');
                    var title = nextItem.data('title');
                    var postLink = nextItem.data('postLink');
                    var callbackFunction = function () {
                        changeState({
                            currentItem: nextItem,
                        });
                    };

                    displayOrUpdateViewBox(
                        imageUrl,
                        title,
                        nextIndex,
                        postLink,
                        callbackFunction,
                    );
                    break;
                case 'prev':
                    var prevIndex = currentItemIndex - 1 >= 0
                        ? currentItemIndex - 1
                        : $galleryItems.length - 1;
                    var prevItem = $galleryItems.filter(function () {
                        return $(this).data('index') === prevIndex
                    });
                    var imageUrl = prevItem.data('large-image-url');
                    var title = prevItem.data('title');
                    var postLink = prevItem.data('postLink');
                    var callbackFunction = function () {
                        changeState({
                            currentItem: prevItem,
                        })
                    };

                    displayOrUpdateViewBox(
                        imageUrl,
                        title,
                        prevIndex,
                        postLink,
                        callbackFunction,
                    );
                    break;
                case 'index':
                    var nextIndex = buttonData.index;
                    var nextItem = $galleryItems.filter(function () {
                        return $(this).data('index') === nextIndex
                    });
                    var imageUrl = nextItem.data('large-image-url');
                    var title = nextItem.data('title');
                    var postLink = nextItem.data('postLink');
                    var callbackFunction = function () {
                        changeState({
                            currentItem: nextItem,
                        })
                    };
                    displayOrUpdateViewBox(
                        imageUrl,
                        title,
                        prevIndex,
                        postLink,
                        callbackFunction,
                    );
                    break;
                default:
                    return;
            }
        }

        function createNavigation(galleryItems) {
            var isWithThumbnails = settings.isWithThumbnails;
            var $navigationList = $('<ul class="sp-easygallery-viewbox__navigation__menu"></ul>');
            var $navigationListItems = $.map(galleryItems, function (item) {
                var $item = $(item);
                var $navigationListItem = $('<li class="sp-easygallery-viewbox__navigation__menu__item"></li>');
                var navigationTargetIndex = $item.data('index');
                var $button = $('<button type="button"></button>');
                $button.attr('data-target', navigationTargetIndex);
                $button.attr('aria-label', 'Navigate to image ' + (navigationTargetIndex + 1));
                $button.on('click', function () {
                    var index = $(this).data('target');
                    changeImage({
                        type: 'index',
                        index: index,
                    });
                });

                if (isWithThumbnails) {
                    var $image = $item.find('img').clone();
                    $navigationListItem.addClass('with-thumbnail');
                    $button.append($image);
                }

                $navigationListItem.append($button);

                return $navigationListItem;

            });

            $navigationList.append($navigationListItems);

            return $navigationList;
        }

        function createControlButtons() {
            const $controlButtonPrev = $('<button class="sp-easy-gallery-viewbox-controls-buttons prev" type="button"></button>');
            const $controlButtonNext = $('<button class="sp-easy-gallery-viewbox-controls-buttons next" type="button"></button>');

            $controlButtonPrev.on('click', function () {
                changeImage({
                    type: 'prev',
                });
            });

            $controlButtonNext.on('click', function () {
                changeImage({
                    type: 'next',
                });
            });

            $viewBoxWrapper
                .append($controlButtonPrev)
                .append($controlButtonNext);
        }

        function displayOrUpdateViewBox(imageUrl, title, index, postLink, callback) {
            var $alreadyDisplayedViewBox = getAlreadyDisplayedViewBox();
            var viewBoxAlreadyExists = $alreadyDisplayedViewBox.length > 0;
            var $itemLink = $itemInformations.find('.sp-easygallery__post-link--button');
            $title.text(title);
            $img.attr('src', imageUrl);
            $img.attr('alt', title);
            $img.data('index', index);
            $itemLink.attr('href', postLink);


            var $alreadyDisplayedViewboxImage = $alreadyDisplayedViewBox
                .find('img.sp-easy-gallery-viewbox__image');
            var $alreadyDisplayedViewBoxTitle = $alreadyDisplayedViewBox
                .find('.sp-easy-gallery-viewbox__item__title');

            if (viewBoxAlreadyExists) {

                $alreadyDisplayedViewboxImage.replaceWith($img);
                $alreadyDisplayedViewBoxTitle.replaceWith($title);

                if (callback) {
                    callback();
                }

            } else {
                $viewBoxImageContainer.append($img);
                $viewBoxWrapperInner
                    .append($viewBoxImageContainer)
                    .append($title)
                    .append($itemInformations);
                createControlButtons();
                var $viewBox = $viewBoxWrapper
                    .append($viewBoxCloseButton)
                    .append($viewBoxWrapperInner);
                var $navigation = createNavigation(galleryItems);
                $viewBox.append($navigation);
                $('body')
                    .append($viewBox)
                    .addClass('overflow-hidden')
                    .css('overflow', 'hidden');

                mobileHooks();

                $viewBox.fadeIn(200, function () {
                    $viewBox.focus();
                    if (callback) {
                        callback();
                    }
                });
            }

            $viewBoxCloseButton.on('click', onClickCloseButton);

        };

        function onSelectItem(item) {
            var $selectedItem = $(item);
            var largeImageUrl = $selectedItem.data('large-image-url');
            var title = $selectedItem.data('title');
            var postLink = $selectedItem.data('postlink');
            console.log(postLink);

            var index = $selectedItem.data('index');
            displayOrUpdateViewBox(largeImageUrl, title, index, postLink, function () {
                changeState({
                    currentItem: $selectedItem,
                });
            });
        }

        function displayViewBoxByPostURL(url) {
            var isGalleryPost = new RegExp(/eg-item/, 'gi');
            if (isGalleryPost.test(url)) {
                var splittedString = url.split('/').filter(str => str !== '');
                var urlPostTitle = splittedString[splittedString.length - 1];
                var $urlSelectedPost = $(galleryItems).filter(function () {
                    return $(this).data('title').toLowerCase() === urlPostTitle.replace(/-/g, ' ');
                });

                if ($urlSelectedPost.length) {
                    var index = $urlSelectedPost.data('index');
                    changeImage({
                        type: 'index',
                        index: index,
                    });
                }

            }
        }
        $.fn.easyGalleryViewBox.displayViewBoxByPostURL = function (url) {
            displayViewBoxByPostURL(url);
        }

        var init = function () {

            if (settings.autoDisplayViewBoxWithURLChange) {
                displayViewBoxByPostURL(window.location.href);
            }

            $(window).on('resize orientationchange', function () {
                setTimeout(function () {
                    mobileHooks();
                }, 100);
            });

            $(document).on('keydown', function (event) {
                var isViewBoxDisplayed = $('.sp-easy-gallery-viewbox').length > 0;
                switch (event.which) {
                    case 27:
                        closeViewBox();
                        break;

                    case 39:
                        if (isViewBoxDisplayed) {
                            changeImage({
                                type: 'next',
                            });
                        }

                        break;
                    case 37:
                        if (isViewBoxDisplayed) {
                            changeImage({
                                type: 'prev',
                            });
                        }
                        break;
                    default:
                        return;
                }
            });

            return galleryItems.each(function (i) {
                var $item = $(this);
                var viewBoxTrigger = settings.viewBoxTrigger;
                var clickTarget = viewBoxTrigger
                    ? $(viewBoxTrigger)
                    : $item;
                clickTarget.on('click keydown', function (event) {
                    if (event.type === 'click' || event.which === 13) {
                        onSelectItem(this);
                    }
                });
            });
        }

        init();


    }

    function masonryInit() {
        $(gallerySelector).masonry({
            // options
            itemSelector: galleryItemSelector,
        });
    }

    function viewBoxInit() {
        $(galleryItemSelector).easyGalleryViewBox({
            isWithThumbnails: true,
            autoDisplayViewBoxWithURLChange: true,
        });
    }

    $(document).ready(function () {
        $(gallerySelector).loader();


        $(gallerySelector).imagesLoaded(function () {
            $(galleryItemSelector).css('opacity', 0);
            $(gallerySelector).loader.removeLoader();
            masonryInit();
            $(galleryItemSelector)
                .addClass('visible')
                .css('opacity', 1);
            viewBoxInit();
        });

    });
})(jQuery);